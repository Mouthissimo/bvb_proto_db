-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 07, 2020 at 09:32 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bvb_proto_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `robots_table`
--

DROP TABLE IF EXISTS `robots_table`;
CREATE TABLE IF NOT EXISTS `robots_table` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `wins` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `losses` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `stat_attack` int(11) UNSIGNED NOT NULL,
  `stat_hp` int(11) UNSIGNED NOT NULL,
  `stat_speed` int(11) UNSIGNED NOT NULL,
  `behavior_proximity` int(11) UNSIGNED NOT NULL,
  `behavior_agility` int(11) UNSIGNED NOT NULL,
  `behavior_aggressivity` int(11) UNSIGNED NOT NULL,
  `weapon_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `robot_weapon_id_TO_weapon_id` (`weapon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `robots_table`
--

INSERT INTO `robots_table` (`id`, `wins`, `losses`, `stat_attack`, `stat_hp`, `stat_speed`, `behavior_proximity`, `behavior_agility`, `behavior_aggressivity`, `weapon_id`) VALUES
(1, 0, 0, 30, 50, 40, 0, 40, 50, 1),
(2, 0, 0, 10, 70, 20, 0, 20, 10, 2);

-- --------------------------------------------------------

--
-- Table structure for table `weapons_table`
--

DROP TABLE IF EXISTS `weapons_table`;
CREATE TABLE IF NOT EXISTS `weapons_table` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bullet` int(11) UNSIGNED NOT NULL,
  `bullet_speed` int(11) UNSIGNED NOT NULL,
  `max_range` int(11) UNSIGNED NOT NULL,
  `min_range` int(11) UNSIGNED NOT NULL,
  `rate_of_fire` int(11) UNSIGNED NOT NULL,
  `damage_value` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `weapons_table`
--

INSERT INTO `weapons_table` (`id`, `bullet`, `bullet_speed`, `max_range`, `min_range`, `rate_of_fire`, `damage_value`) VALUES
(1, 1, 4, 8, 4, 4, 2),
(2, 1, 2, 14, 8, 1, 4);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `robots_table`
--
ALTER TABLE `robots_table`
  ADD CONSTRAINT `robot_weapon_id_TO_weapon_id` FOREIGN KEY (`weapon_id`) REFERENCES `weapons_table` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
