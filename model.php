<?php

  //  __Connection à la base de données__ (maison)
  //  http://localhost/bvb_proto_db/
  const C_HOST = 'localhost';
  const C_DBNAME = 'bvb_proto_db';
  const C_LOGIN = 'root';
  const C_PASSWORD = 'root';

  //  __Connection à la base de données__ (coding factory)
  //  http://localhost:8888/bvb_proto_db/
  //  const C_HOST = 'localhost';
  //  const C_DBNAME = 'bvb_proto_db';
  //  const C_LOGIN = 'root';
  //  const C_PASSWORD = 'nite5club';


  //connection to database
  function dbconnect()
  {
    try
    {
      $bdd = new PDO('mysql: host='.C_HOST.'; dbname='.C_DBNAME.'; charset=utf8', C_LOGIN, C_PASSWORD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    }
    catch (Exception $e)
    {
      die('Erreur : ' . $e->getMessage());
    }

    return $bdd;
  }

  
  function getRobotData($inputRobotId)
  {
    $bdd = dbconnect();
    $robotDataQuerry = 'SELECT '
                        . 'stat_attack, '
                        . 'stat_hp, '
                        . 'stat_speed, '
                        . 'behavior_proximity, '
                        . 'behavior_agility, '
                        . 'behavior_aggressivity '
                     . 'FROM robots_table '
                     . 'WHERE id = :req_robot_id';
    
    try
    {
        $req = $bdd->prepare($robotDataQuerry);
        $req->execute(array('req_robot_id' => htmlspecialchars($inputRobotId)));
        $data = $req->fetch();
        $req->closeCursor();
    }
    catch (Exception $e)
    {
      die('Erreur : ' . $e->getMessage());
    }
    
    return $data;
  }
  
  
  function getWeaponDataFromRobotId($inputRobotId)
  {
    $bdd = dbconnect();
    $weaponDataFromRobotIdQuerry = 'SELECT '
                                    . 'weapons_table.bullet, '
                                    . 'weapons_table.bullet_speed, '
                                    . 'weapons_table.max_range, '
                                    . 'weapons_table.min_range, '
                                    . 'weapons_table.rate_of_fire, '
                                    . 'weapons_table.damage_value '
                                 . 'FROM weapons_table '
                                 . 'INNER JOIN robots_table '
                                 . 'ON weapons_table.id = robots_table.weapon_id '
                                 . 'WHERE robots_table.id = :req_robot_id';
    
    try
    {
        $req = $bdd->prepare($weaponDataFromRobotIdQuerry);
        $req->execute(array('req_robot_id' => htmlspecialchars($inputRobotId)));
        $data = $req->fetch();
        $req->closeCursor();
    }
    catch (Exception $e)
    {
      die('Erreur : ' . $e->getMessage());
    }
    
    return $data;
  }
  
  
  function getRobotWinRate($inputRobotId)
  {
    $bdd = dbconnect();
    $winRateDataQuerry = 'SELECT '
                            . 'wins, '
                            . 'losses '
                       . 'FROM robots_table '
                       . 'WHERE id = :req_robot_id';
    
    try
    {
        $req = $bdd->prepare($winRateDataQuerry);
        $req->execute(array('req_robot_id' => htmlspecialchars($inputRobotId)));
        $data = $req->fetch();
        $req->closeCursor();
    }
    catch (Exception $e)
    {
      die('Erreur : ' . $e->getMessage());
    }
    
    return $data;
  }
  
  
  function AddRobotWins($inputRobotId, $updatedRobotWins)
  {   
    $bdd = dbconnect();
    $updateWinsQuerry = 'UPDATE robots_table '
                       .'SET wins = :updated_robot_wins + wins '
                       .'WHERE id = :req_robot_id';
    
    try
    {
        $req = $bdd->prepare($updateWinsQuerry);
        $req->execute(array('updated_robot_wins' => htmlspecialchars($updatedRobotWins),
                            'req_robot_id' => htmlspecialchars($inputRobotId)));
        $req->closeCursor();
    }
    catch (Exception $e)
    {
      die('Erreur : ' . $e->getMessage());
    }
  }
  
  function AddRobotLooses($inputRobotId, $updatedRobotLosses)
  {   
    $bdd = dbconnect();
    $updateWinsQuerry = 'UPDATE robots_table '
                       .'SET losses = :updated_robot_losses + losses '
                       .'WHERE id = :req_robot_id';
    
    try
    {
        $req = $bdd->prepare($updateWinsQuerry);
        $req->execute(array('updated_robot_losses' => htmlspecialchars($updatedRobotLosses),
                            'req_robot_id' => htmlspecialchars($inputRobotId)));
        $req->closeCursor();
    }
    catch (Exception $e)
    {
      die('Erreur : ' . $e->getMessage());
    }
  }

  function updateRobotWins($inputRobotId, $updatedRobotWins)
  {   
    $bdd = dbconnect();
    $updateWinsQuerry = 'UPDATE robots_table '
                       .'SET wins = :updated_robot_wins '
                       .'WHERE id = :req_robot_id';
    
    try
    {
        $req = $bdd->prepare($updateWinsQuerry);
        $req->execute(array('updated_robot_wins' => htmlspecialchars($updatedRobotWins),
                            'req_robot_id' => htmlspecialchars($inputRobotId)));
        $req->closeCursor();
    }
    catch (Exception $e)
    {
      die('Erreur : ' . $e->getMessage());
    }
  }
  
  function updateRobotLosses($inputRobotId, $updatedRobotLosses)
  {   
    $bdd = dbconnect();
    $updateWinsQuerry = 'UPDATE robots_table '
                       .'SET losses = :updated_robot_losses '
                       .'WHERE id = :req_robot_id';
    
    try
    {
        $req = $bdd->prepare($updateWinsQuerry);
        $req->execute(array('updated_robot_losses' => htmlspecialchars($updatedRobotLosses),
                            'req_robot_id' => htmlspecialchars($inputRobotId)));
        $req->closeCursor();
    }
    catch (Exception $e)
    {
      die('Erreur : ' . $e->getMessage());
    }
  }
  
  function resetRobotWinrate($inputRobotId)
  {   
    $bdd = dbconnect();
    $updateWinsQuerry = 'UPDATE robots_table '
                       .'SET '
                       .'wins = 0, '
                       .'losses = 0 '
                       .'WHERE id = :req_robot_id';
    
    try
    {
        $req = $bdd->prepare($updateWinsQuerry);
        $req->execute(array('req_robot_id' => htmlspecialchars($inputRobotId)));
        $req->closeCursor();
    }
    catch (Exception $e)
    {
      die('Erreur : ' . $e->getMessage());
    }
  }

  function getRobotFromStats($stat_hp, $stat_attack, $stat_speed, $behavior_agility, 
  $behavior_proximity, $behavior_aggressivity, $weapon_id )
  {   
    $bdd = dbconnect();
    $weapon_id = 1; // mock
    $doesRobotExist = 'SELECT * FROM robots_table
                              WHERE 
                                    stat_hp = :stat_hp AND
                                    weapon_id = :weapon_id AND
                                    stat_attack = :stat_attack AND
                                    stat_speed = :stat_speed AND
                                    behavior_agility = :behavior_agility AND
                                    behavior_proximity = :behavior_proximity AND
                                    behavior_aggressivity = :behavior_aggressivity';
    
    try
    {
        $req = $bdd->prepare($doesRobotExist);
        $req->execute(array(
          'stat_hp' => htmlspecialchars($stat_hp),
          'weapon_id' => htmlspecialchars($weapon_id),
          'stat_attack' => htmlspecialchars($stat_attack),
          'stat_speed' => htmlspecialchars($stat_speed),
          'behavior_agility' => htmlspecialchars($behavior_agility),
          'behavior_proximity' => htmlspecialchars($behavior_proximity),
          'behavior_aggressivity' => htmlspecialchars($behavior_aggressivity)
      ));
        $data = $req->fetch();
        $req->closeCursor();
    }
    catch (Exception $e)
    {
      die('Erreur : ' . $e->getMessage());
    }
    return $data;
  }

  function insertRobot($wins, $looses, $stat_hp, $stat_attack, $stat_speed, $behavior_agility, 
  $behavior_proximity, $behavior_aggressivity, $weapon_id)
  {
    $bdd = dbconnect();
    $insert = 'INSERT INTO robots_table (wins, losses, stat_hp, weapon_id, stat_attack, stat_speed, behavior_agility , behavior_proximity, behavior_aggressivity)
                      VALUES  (:wins,:losses,:stat_hp,:weapon_id,:stat_attack,:stat_speed, :behavior_agility, :behavior_proximity, :behavior_aggressivity)';
    
    try
    {
        $req = $bdd->prepare($insert);
        $req->execute(array(
          'wins' => htmlspecialchars($wins),
          'losses' => htmlspecialchars($looses),
          'stat_hp' => htmlspecialchars($stat_hp),
          'weapon_id' => htmlspecialchars($weapon_id),
          'stat_attack' => htmlspecialchars($stat_attack),
          'stat_speed' => htmlspecialchars($stat_speed),
          'behavior_agility' => htmlspecialchars($behavior_agility),
          'behavior_proximity' => htmlspecialchars($behavior_proximity),
          'behavior_aggressivity' => htmlspecialchars($behavior_aggressivity)
      ));
        $req->closeCursor();
    }
    catch (Exception $e)
    {
      die('Erreur : ' . $e->getMessage());
    }
  }

?>