<?php

class Robot
{

	//Constructeur
	function __construct ($wins, $looses, $stat_hp, $stat_attack, $stat_speed, $behavior_agility, 
		$behavior_proximity, $behavior_aggressivity, $weapon_id )
	{
		$this->wins = $wins; 
		$this->looses = $looses; 
		$this->stat_hp = $stat_hp; 
		$this->stat_attack = $stat_attack; 
		$this->stat_speed = $stat_speed; 
		$this->behavior_agility = $behavior_agility; 
		$this->behavior_proximity = $behavior_proximity; 
		$this->behavior_aggressivity = $behavior_aggressivity; 
		$this->weapon_id = $weapon_id;
	}

	//attributs #lafusée
	public $wins;
	public $looses;
	public $stat_hp;
	public $stat_speed;
	public $stat_attack;
	public $behavior_agility;
	public $behavior_proximity;
	public $behavior_aggressivity;
	public $weapon_id;

} 
