<?php

  require('model.php');

  $robotId = $_POST["robotId"];
  
  $winRateData = getRobotWinRate($robotId);
  
  $updatedLossesValue = $winRateData[1] + 1;
  
  updateRobotLosses($robotId, $updatedLossesValue);

?>